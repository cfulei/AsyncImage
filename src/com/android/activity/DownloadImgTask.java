package com.android.activity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;


public class DownloadImgTask extends AsyncTask<String, Float, Bitmap> {
	private static final String TAG="DownloadImgTask";    
	private PorterDuffView pdView;
        

        public DownloadImgTask(PorterDuffView pdView) {
                this.pdView = pdView;
        }

        /** 下载准备工作。在UI线程中调用。 */
        protected void onPreExecute() {
        	Log.i(TAG, "onPreExecute");
        }

        /** 执行下载。在背景线程调用。 */
        protected Bitmap doInBackground(String... params) {
        	Log.i(TAG, "doInBackground:" + params[0]);
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(params[0]);
                InputStream is = null;
                ByteArrayOutputStream baos = null;
                try {
                        HttpResponse httpResponse = httpClient.execute(httpGet);
                        printHttpResponse(httpResponse);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        long length = httpEntity.getContentLength();
                        Log.i(TAG, "content length=" + length);
                        is = httpEntity.getContent();
                        if (is != null) {
                                baos = new ByteArrayOutputStream();
                                byte[] buf = new byte[128];
                                int read = -1;
                                int count = 0;
                                while ((read = is.read(buf)) != -1) {
                                        baos.write(buf, 0, read);
                                        count += read;
                                        publishProgress(count * 1.0f / length);
                                }
                                Log.i(TAG, "count=" + count + " length=" + length);
                                byte[] data = baos.toByteArray();
                                Bitmap bit = BitmapFactory.decodeByteArray(data, 0, data.length);
                                return bit;
                        }
                } catch (ClientProtocolException e) {
                        e.printStackTrace();
                } catch (IOException e) {
                        e.printStackTrace();
                } finally {
                        try {
                                if (baos != null) {
                                        baos.close();
                                }
                                if (is != null) {
                                        is.close();
                                }
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
                return null;
        }

        /** 更新下载进度。在UI线程调用。onProgressUpdate */
        protected void onProgressUpdate(Float... progress) {
                // LogOut.out(this, "onProgressUpdate");
                pdView.setProgress(progress[0]);
        }

        /** 通知下载任务完成。在UI线程调用。 */
        protected void onPostExecute(Bitmap bit) {
        		Log.i(TAG, "onPostExecute");
                pdView.setPorterDuffMode(false);
                pdView.setLoading(false);
                pdView.setImageBitmap(bit);
        }

        protected void onCancelled() {
        		Log.i(TAG, "DownloadImgTask cancel...");
                super.onCancelled();
        }

        private void printHttpResponse(HttpResponse httpResponse) {
                Header[] headerArr = httpResponse.getAllHeaders();
                for (int i = 0; i < headerArr.length; i++) {
                        Header header = headerArr[i];
                        Log.i(TAG, "name[" + header.getName() + "]value[" + header.getValue() + "]");
                }
                HttpParams params = httpResponse.getParams();
                Log.i(TAG, String.valueOf(params));
                Log.i(TAG, String.valueOf(httpResponse.getLocale()));
        }
}
